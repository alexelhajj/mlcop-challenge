#!/usr/bin/env python
# coding: utf-8

# **Project title :- Bike Renting using Python**

# **Problem statement :-**
# **Exploratory Data Analysis**

# **Import the required libraries**

from copy import deepcopy
import numpy as np
import pandas as pd
from sklearn.metrics import r2_score
from sklearn.model_selection import train_test_split
from evaluate import get_eval, score
import warnings
warnings.filterwarnings('ignore')
import os
print(os.listdir("./data"))


# **Read the training data**
#import the csv file
bikeTrain = pd.read_csv("./data/TrainSeoulBikeData.csv")
bikeTrain.drop('ID',inplace=True,axis=1)

bikeTest = pd.read_csv("./data/TestSeoulBikeData.csv")
bikeTest.drop('ID',inplace=True,axis=1)

bike = bikeTrain.append(bikeTest)
bike.reset_index(inplace=True)
bike.drop('index',inplace=True,axis=1)

bike.rename(columns={'Rented Bike Count':'Rented_Bike_Count',
                     'Temperature(°C)':'Temperature',
                     'Humidity(%)':'Humidity',
                     'Wind speed (m/s)':'Wind_speed',
                     'Visibility (10m)':'Visibility',
                     'Dew point temperature(°C)':'Dew_point_temp',
                     'Solar Radiation (MJ/m2)':'Solar_Radiation',
                     'Snowfall (cm)':'Snowfall',
                     'Rainfall(mm)':'Rainfall',
                     'Functioning Day':'Functioning_Day'
                     },inplace=True)


#Shape of the dataset
bike.shape

# The dataset contains 7,896 observations and 14 attributes.

# **Data types**
bike.dtypes

#Read the data
bike.head(5)
bike.columns


bike_df=deepcopy(bike)

#Rename the columns
#Read the data
bike_df.head(5)
bike_df.columns


# **Typecasting the datetime and numerical attributes**
bike_df['Date']=pd.to_datetime(bike_df.Date)
bike_df['day'] = bike_df['Date'].dt.day
bike_df['month'] = bike_df['Date'].dt.month
bike_df['year'] = bike_df['Date'].dt.year

bike_df['DayofWeek'] = pd.DatetimeIndex(bike_df['Date']).day_name()
bike_df['weekend'] = np.where((bike_df['DayofWeek'] == "Saturday") | \
                              (bike_df['DayofWeek'] == "Sunday"),1,0)
bike_df['weekday'] = np.where((bike_df['DayofWeek'] != "Saturday") & \
                              (bike_df['DayofWeek'] != "Sunday"),1,0)

bike_df["Holiday"] = bike_df["Holiday"].astype('category')    
bike_df["Functioning_Day"] = bike_df["Functioning_Day"].astype('category')    
bike_df["Holiday"] = bike_df["Holiday"].cat.codes
bike_df["Functioning_Day"] = bike_df["Functioning_Day"].cat.codes
bike_df["Holiday"] = bike_df["Holiday"].astype('int64')    
bike_df["Functioning_Day"] = bike_df["Functioning_Day"].astype('int64')

    
#Read the data
bike_df.head(5)

#Summary of the dataset
bike_df.describe()
bike_df.columns

# **Missing value analysis**
#Missing values in dataset
bike_df.isnull().sum()
bike_df.columns
bike_df.DayofWeek.value_counts()
bike_df["Seasons"]= bike_df["Seasons"].map({"Winter":1,"Summer":2,"Spring":3,"Autumn":4})
bike_df["DayofWeek"]= bike_df["DayofWeek"].map({"Monday":1,"Tuesday":2,
                                                "Wednesday":3,"Thursday":4,
                                                "Friday":5,"Saturday":6,
                                                "Sunday":7})


#load the required libraries
from sklearn import preprocessing,metrics,linear_model
from sklearn.model_selection import cross_val_score,cross_val_predict, \
    train_test_split

# Split the dataset into train and test in the ratio of 70:30
bikeTrain = bike_df[pd.notnull(bike_df['Rented_Bike_Count'])].sort_values(by=["Date"])
bikeTest = bike_df[~pd.notnull(bike_df['Rented_Bike_Count'])].sort_values(by=["Date"])

# MODELLING THE DATA
X = bikeTrain.drop(['Date',"Rented_Bike_Count"],axis=1)
y = bikeTrain["Rented_Bike_Count"]


X_train,X_test,y_train,y_test = train_test_split(X,y)


# LINEAR REGRESSION MODEL
from sklearn.linear_model import LinearRegression 
lr = LinearRegression() 
lr.fit(X_train,y_train)

prediction = lr.predict(X_test)

metrics.r2_score(y_test,prediction)

# **Accuracy of model**
#Accuracy of the model
lrr=lr.score(X_train,y_train)
print('Accuracy of the model :',lrr)
print('Model coefficients :',lr.coef_)
print('Model intercept value :',lr.intercept_)

# **Cross validation prediction**
#Cross validation prediction
predict=cross_val_predict(lr,X_train,y_train,cv=3)
predict

# **Model evalution metrics**
# **R-squared and mean squared error score**
#R-squared scores
r2_scores = cross_val_score(lr, X_train, y_train, cv=3)
print('R-squared scores :',np.average(r2_scores))

from sklearn.tree import DecisionTreeRegressor
dtr=DecisionTreeRegressor()
dtr.fit(X_train,y_train)

Pred=dtr.predict(X_test)
metrics.r2_score(y_test,Pred)

from sklearn.ensemble import RandomForestRegressor

rf=RandomForestRegressor()
rf.fit(X_train,y_train)

prediction=rf.predict(X_test)

metrics.r2_score(y_test,prediction)

bikeTest.columns
bikeTest = bikeTest.reset_index()
bikeTest.rename(columns={'index':'Id'},inplace=True)

#using the most accurate model in this case , random forest regresson
t = bikeTest.drop(["Date", "Rented_Bike_Count", "Id"],axis=1)

prediction=rf.predict(t)
results_df= pd.DataFrame({'Id':bikeTest['Id'] ,'Rented_Bike_Count': prediction})
results_df = results_df.sort_values(by=['Id'])
results_df.set_index("Id").head(5)

results_df.to_csv('./data/subM.csv', index=False)
print("your submission is succesfull")



################################################################
# def score(yhat, y):
#     # return float(np.mean((yhat - y)**2))
#     return r2_score(yhat, y)

# #note that if an error is thrown in this fxn 
# #then the user will get a flash() that upload failed
# def loadAndScore(fullPath, unique_name): # enter your unique username here
#     """function loads the dataset from filePath and scores submission
#     against the solution file
#     function returns tuple in format public, private, total loss function score"""
#     #in a real implementation of this app add more QA in this fxn
#     sub = pd.read_csv(fullPath)
#     ans = get_eval() # Evaluation dataset
#     public_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 1],
#                          ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 1])
#     private_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 0],
#                          ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 0])
#     total_score = score(sub.Rented_Bike_Count,
#                          ans.Rented_Bike_Count)
#     return (public_score, private_score, total_score, unique_name)



