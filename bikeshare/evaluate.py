#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  9 18:11:34 2022

"""

import tempfile
import errno
import shutil
import os
import pandas as pd
from sklearn.metrics import r2_score

def score(yhat, y):
    # return float(np.mean((yhat - y)**2))
    return r2_score(yhat, y)


def get_eval(fullPath, unique_name):

    """
    Creates test repository with single commit and return its path
    
    Function loads the dataset from filePath and scores submission
        against the solution file
    
    Parameters
    ----------
    fullPath : str
        Full path to predicted results.
        Example: /Users/username/mlcop/bikeshare/pred.csv
    unique_name : str
        Unque name of participant.
        Please enter in a unique name that will not be duplicated.
            Ex: firstname_lastname or 
                firstname_{set-of-numbers} i.e., firstname_123

    Returns
    -------
    public_score : int
        Public score of submission.
    private_score : int
        Private score of submission.
    total_score : int
        Total score of submission.
    unique_name : str
        Unique name of participant.

    """
    try:
        temporary_dir = tempfile.mkdtemp()
        os.chdir(temporary_dir)
        print(temporary_dir)
        filename = os.path.basename(temporary_dir)
        print(filename)
        
        os.system("git clone https://gitlab.com/alexelhajj/testmlcop.git testmlcop")
        os.chdir('testmlcop')
        os.system("pwd")
        os.system("ls")
        sub = pd.read_csv(fullPath)
        ans = pd.read_csv("y_test_SeoulBikeData_raw.csv")
        public_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 1],
                             ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 1])
        private_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 0],
                              ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 0])
        total_score = score(sub.Rented_Bike_Count,
                            ans.Rented_Bike_Count)
    finally:
        try:
            shutil.rmtree(temporary_dir)  # delete directory
        except OSError as exc:
            if exc.errno != errno.ENOENT:  # ENOENT - no such file or directory
                raise  # re-raise exception

    return (public_score, private_score, total_score, unique_name)





