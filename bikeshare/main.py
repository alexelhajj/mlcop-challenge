#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  2 19:51:22 2022

"""


# from pipeline import get_score
# from Example_Submission import loadAndScore # replace with working code
# from template import loadAndScore # replace with working code
from evaluate import get_eval
import pandas as pd
print(pd.__version__)
import os

# Get the current working directory
cwd = os.getcwd()

# Print the current working directory
print("Current working directory: {0}".format(cwd))

# Import leaderboard
master_pd = pd.read_csv("leaderboard.csv")
master_dict = master_pd.to_dict('records')

# Get score and unique name
public_score, private_score, total_score, unique_name = get_eval('/Users/alexanderel-hajj/Documents/MLCOP_CHALLENGE/mlcop-challenge/bikeshare/data/subM.csv', "Alex_123")

# Create new entry to master leaderboard
new_entry = {unique_name: total_score}

# check leaderboard and keep largest score
def check_username(master, new_entry):
    print(f"New Entry: {new_entry}")
    username = [x for x,y in new_entry.items()][0]
    print(username)
    new_score = [y for x,y in new_entry.items()][0]
    print(new_score)
    if username in [list(y.items())[0][1] for x,y in enumerate(master)]:
        print('name found')
        print('check if score is better')
        print(f'new score: {new_score}')
        if new_score > [list(y.items())[1] for x,y in enumerate(master_dict) \
                        if username in list(y.items())[0]][0][1]:
            print('yes')
            print(new_score)
            index = [x for x,y in enumerate(master_dict) if username in \
                     list(y.items())[0]][0]
            master[index] = {'name' : username, 'score' : new_score}
        else:
            print('waaaa')
    else:
        print('username not in list')
        print(f"New Entry: {new_entry}")
        master.append({'name': username, 'score': new_score})
    return master

# Change back to working directory
os.chdir(cwd)
cwd_new = os.getcwd()
# Print the current working directory
print("Current working directory: {0}".format(cwd_new))

bshh = check_username(master_dict, new_entry)
bshh_pd = pd.DataFrame(bshh)
bshh_pd = bshh_pd.sort_values(by='score', axis=0, ascending=False)
print("Exporting leaderboard.csv")
bshh_pd.to_csv("leaderboard.csv", index=False)


