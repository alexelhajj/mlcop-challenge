#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  9 18:59:17 2022

"""


"""
1. Import Modules
"""


"""
2. Import Data
"""


"""
3. Feature Engineering
    - create derived variables
    - deal with missing data
    - split data into train/test
"""


"""
4. Modelling
    - Linear regression
    - Random forest
    
    - Training/Prediction/Metrics
"""


"""
5. Validation
    - Cross-validation
"""


"""
6. Save prediction results
"""


"""
7. Submit evaluation score
"""
from sklearn.metrics import r2_score
import pandas as pd
from evaluate import get_eval

################################################################
# def score(yhat, y):
#     # return float(np.mean((yhat - y)**2))
#     return r2_score(yhat, y)

# #note that if an error is thrown in this fxn 
# #then the user will get a flash() that upload failed
# # enter your unique username here
# def loadAndScore(fullPath, unique_name):
#     """
#     Function loads the dataset from filePath and scores submission
#     against the solution file
    
#     Parameters
#     ----------
#     fullPath : str
#         Full path to predicted results.
#         Example: /Users/username/mlcop/bikeshare/pred.csv
#     unique_name : str
#         Unque name of participant.
#         Please enter in a unique name that will not be duplicated.
#             Ex: firstname_lastname or 
#                 firstname_{set-of-numbers} i.e., firstname_123

#     Returns
#     -------
#     public_score : int
#         Public score of submission.
#     private_score : int
#         Private score of submission.
#     total_score : int
#         Total score of submission.
#     unique_name : str
#         Unique name of participant.

#     """
    
    
#     #in a real implementation of this app add more QA in this fxn
#     sub = pd.read_csv(fullPath)
#     ans = get_eval() # Evaluation dataset
#     public_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 1],
#                          ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 1])
#     private_score = score(sub.Rented_Bike_Count[ans.PublicLeaderboardInd == 0],
#                           ans.Rented_Bike_Count[ans.PublicLeaderboardInd == 0])
#     total_score = score(sub.Rented_Bike_Count,
#                         ans.Rented_Bike_Count)
#     return (public_score, private_score, total_score, unique_name)



